import React from 'react';
import styled from 'styled-components';

const StyledSVG = styled.svg`
  height: 28px;
  width: 28px;
  border: 0 none;
`;

const Spinner = () => (
  <StyledSVG
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 100 100"
    preserveAspectRatio="xMidYMid"
    className="lds-reload"
    style={{ background: 'none' }}
  >
    <g transform="rotate(257.903 50 50)">
      <path
        d="M50 15A35 35 0 1 0 74.787 25.213"
        fill="none"
        ng-attr-stroke="black"
        ng-attr-stroke-width="4"
        stroke="#1d0e0b"
        strokeWidth="6"
      />
      <path
        ng-attr-d="12"
        ng-attr-fill="black"
        d="M49 6L49 24L58 15L49 6"
        fill="#1d0e0b"
      />
      <animateTransform
        attributeName="transform"
        type="rotate"
        calcMode="linear"
        values="0 50 50;360 50 50"
        keyTimes="0;1"
        dur="1s"
        begin="0s"
        repeatCount="indefinite"
      />
    </g>
  </StyledSVG>
);

export default Spinner;
