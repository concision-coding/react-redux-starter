import React from 'react';

const CloseIcon = () => (
  <svg viewBox="0 0 32 32">
    <g fillRule="evenodd">
      <path
        d="M17.842 15.538l5.83-5.828a1.476 1.476 0 1 0-2.09-2.089l-5.828 5.83-5.829-5.83A1.476 1.476 0 1 0 7.837 9.71l5.829 5.828-5.83 5.83a1.476 1.476 0 1 0 2.09 2.088l5.828-5.83 5.829 5.83a1.472 1.472 0 0 0 2.088 0 1.476 1.476 0 0 0 0-2.089l-5.829-5.829z"
        id="close-a"
      />
    </g>
  </svg>
);

export default CloseIcon;
