import styled from 'styled-components';

const ToolTip = styled.span`
  font-family: "Open Sans", "Segoe UI", Tahoma, sans-serif;
  background-color: rgb(255, 255, 255);
  border-radius: 4px;
  box-shadow: rgba(0, 0, 0, 0.2) 0px 3px 7px 0px;
  color: rgb(51, 51, 51);
  cursor: pointer;
  display: block;
  font-size: 14px;
  font-weight: 400;
  height: 36px;
  line-height: 20px;
  margin-bottom: 16px;
  padding: 8px;
  position: absolute;
  text-align: center;
  text-decoration-color: rgb(51, 51, 51);
  white-space: nowrap;
  z-index: 2;
  ::before {
    content: "";
    position: absolute;
    right: 20px;
    bottom: -8px;
    width: 16px;
    height: 16px;
    background: white;
    transform: rotate(45deg);
  }
`;

export default ToolTip;
