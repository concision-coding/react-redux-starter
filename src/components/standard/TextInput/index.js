import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const MainLabel = styled.label``;

const LabelTitle = styled.span`
  color: rgb(68, 68, 68);
  cursor: default;
  display: block;
  font-family: "Open Sans", "Segoe UI", Tahoma, sans-serif;
  font-size: 14px;
  font-weight: 400;
  height: 24px;
  line-height: 20px;
  padding-bottom: 4px;
  width: 100%;
`;

const InputWrapper = styled.div`
  position: relative;
  width: 100%;
  > button {
    position: absolute;
    right: 0;
    top: 0;
    margin: 10px;
  }
`;

const StyledInput = styled.input`
  background-color: rgb(255, 255, 255);
  border: 1px solid rgb(200, 174, 91);
  border-radius: 4px;
  color: rgb(0, 0, 0);
  cursor: text;
  display: inline-block;
  font-family: "Open Sans", "Segoe UI", Tahoma, sans-serif;
  font-size: 16px;
  height: 48px;
  line-height: 24px;
  min-width: 49px;
  padding: 8px 16px;
  width: 100%;
  outline-style: none;
  &:focus {
    border-color: #0f6193;
  }
`;

const TextInput = ({ title, rightButton, ...props }) => (
  <MainLabel>
    <LabelTitle>{title}</LabelTitle>
    <InputWrapper>
      <StyledInput {...props} />
      {rightButton}
    </InputWrapper>
  </MainLabel>
);

TextInput.propTypes = {
  title: PropTypes.string.isRequired,
  rightButton: PropTypes.node,
};

TextInput.defaultProps = {
  rightButton: null,
};


export default TextInput;
