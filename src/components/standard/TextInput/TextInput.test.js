import React from 'react';
import 'jest-styled-components';
import { configure, render } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import TextInput from './index';

// React Enzyme adapter
configure({ adapter: new Adapter() });

describe('Components/TextInput', () => {
  it('should match the snapshot', () => {
    const wrapper = render(<TextInput title="" />);
    expect(toJson(wrapper)).toMatchSnapshot();
  });
});
