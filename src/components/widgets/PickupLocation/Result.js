import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const pillMap = {
  t: { name: 'Station', color: 'rgb(91, 91, 91)' },
  c: { name: 'City', color: 'rgb(10, 99, 176)' },
  a: { name: 'Airport', color: 'rgb(150, 20, 18)' },
  d: { name: 'District', color: 'rgb(1, 124, 68)' },
  f: { name: 'Region', color: 'rgb(241, 199, 76)' },
  p: { name: 'Region', color: 'rgb(241, 199, 76)' },
};

const StyledListItem = styled.li`
  border-top: 1px solid #b4b4b4;
  color: rgb(51, 51, 51);
  cursor: pointer;
  display: flex;
  font-family: "Open Sans", "Segoe UI", Tahoma, sans-serif;
  top: -1px;
  position: relative;
  background-color: ${props => (props.hovered ? 'rgb(231, 244, 254)' : 'white')};
`;

const PlaceType = styled.span`
  padding: 8px 16px;
`;

const Pill = styled.span`
  background-color: ${props => props.color};
  border-radius: 4px;
  color: rgb(255, 255, 255);
  cursor: pointer;
  display: block;
  font-size: 12px;
  font-weight: 600;
  height: 22px;
  line-height: 20px;
  margin-top: 2px;
  min-width: 60px;
  padding: 1px 4px;
  text-align: center;
  width: 60px;
`;

const Details = styled.div`
  padding: 8px 16px 8px 0;
  color: rgb(51, 51, 51);
`;

const Name = styled.span`
  font-size: 14px;
  line-height: 1.25rem;
  display: block;
`;

const Region = styled.span`
  font-size: 12px;
  line-height: 1.25rem;
  display: block;
`;

const Mark = styled.span`
  background-color: yellow;
  text-transform: capitalize;
`;

const Result = ({
  index,
  result,
  clickHandler,
  hoverIndex,
  hoverHandler,
  currentValue,
}) => {
  const placeType = result.placeType
    ? pillMap[result.placeType.toLowerCase()]
    : null;
  const matches = currentValue
    ? result.name.toLowerCase().indexOf(currentValue.toLowerCase()) === 0
    : null;
  const name = matches ? (
    <Fragment>
      <Mark id="t_mark">{currentValue}</Mark>
      {result.name.slice(currentValue.length, 100)}
    </Fragment>
  ) : (
    result.name
  );
  return (
    <StyledListItem
      id="t_result"
      onClick={() => clickHandler(result)}
      onMouseOver={() => hoverHandler(index)}
      onFocus={() => hoverHandler(index)}
      focused={false}
      hovered={hoverIndex === index}
    >
      {placeType ? (
        <PlaceType id="t_placetype">
          <Pill color={placeType.color}>{placeType.name}</Pill>
        </PlaceType>
      ) : null}
      <Details>
        <Name>{name}</Name>
        {result.region ? <Region id="t_region">{result.region}</Region> : null}
      </Details>
    </StyledListItem>
  );
};


Result.propTypes = {
  index: PropTypes.number.isRequired,
  result: PropTypes.shape({
    name: PropTypes.string.isRequired,
    placeType: PropTypes.string,
    region: PropTypes.string,
  }).isRequired,
  clickHandler: PropTypes.func.isRequired,
  hoverHandler: PropTypes.func.isRequired,
  currentValue: PropTypes.string,
  hoverIndex: PropTypes.number,
};

Result.defaultProps = {
  currentValue: null,
  hoverIndex: 0,
};


export default Result;
