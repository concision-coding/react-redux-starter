import React from 'react';
import 'jest-styled-components';
import { configure, shallow, render } from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import Result from './Result';
import ResultsList from './ResultsList';
import LocationMask from './LocationMask';
import SearchButton from './SearchButton';
import PickupLocation from './index';
import { mockResults } from './PickupLocation.stories';

// React Enzyme adapter
configure({ adapter: new Adapter() });

describe('Components/PickupLocation', () => {
  describe('/index', () => {
    const defaultProps = {
      value: undefined,
      onChange: () => {},
      onSearch: () => {},
      hoverIndex: 0,
      onFocus: () => {},
      onBlur: () => {},
      onHover: () => {},
      onSelect: () => {},
      onKeyEvent: () => {},
      onClearLocation: () => {},
      onLocate: () => {},
      results: [],
      error: null,
      hasLocation: false,
    };

    it('should match the snapshot', () => {
      const wrapper = render(<PickupLocation {...defaultProps} />);
      expect(toJson(wrapper)).toMatchSnapshot();
    });
    it('should call a function on the value of the input changing', () => {
      const mockFn = jest.fn();
      const component = shallow(<PickupLocation {...defaultProps} onChange={mockFn} />);
      const wrapper = component.find('#t_search_input');
      wrapper.simulate('change');
      expect(mockFn).toHaveBeenCalledTimes(1);
    });
    it('should render a location mask when the hasLocation prop is set', () => {
      const wrapper = shallow(<PickupLocation {...defaultProps} hasLocation />);
      expect(wrapper.find('#t_location')).toHaveLength(1);
    });
    it('should render an error component when one is present', () => {
      const wrapper = shallow(<PickupLocation {...defaultProps} error="an error" />);
      expect(wrapper.find('#t_error').text()).toEqual('an error');
    });
  });

  describe('/Result', () => {
    const defaultProps = {
      index: 1,
      result: {
        name: 'man',
      },
      clickHandler: () => {},
      hoverHandler: () => {},
      currentValue: null,
      focused: false,
    };

    it('should match the snapshot', () => {
      const wrapper = shallow(<Result {...defaultProps} />);
      expect(toJson(wrapper)).toMatchSnapshot();
    });
    it('should call a function on clicking the result', () => {
      const mockFn = jest.fn();
      const component = shallow(<Result {...defaultProps} clickHandler={mockFn} />);
      const button = component.find('#t_result');
      button.simulate('click');
      expect(mockFn).toHaveBeenCalledTimes(1);
    });
    it('should call a function on hovering over the result', () => {
      const mockFn = jest.fn();
      const component = shallow(<Result {...defaultProps} hoverHandler={mockFn} />);
      const wrapper = component.find('#t_result');
      wrapper.simulate('mouseover');
      expect(mockFn).toHaveBeenCalledTimes(1);
    });
    it('should call a function on focusing on the result', () => {
      const mockFn = jest.fn();
      const component = shallow(<Result {...defaultProps} hoverHandler={mockFn} />);
      const wrapper = component.find('#t_result');
      wrapper.simulate('focus');
      expect(mockFn).toHaveBeenCalledTimes(1);
    });
  });

  describe('/ResultsList', () => {
    const defaultProps = {
      currentValue: null,
      onHover: () => {},
      onSelect: () => {},
      showResults: true,
      results: [],
    };
    it('should match the snapshot', () => {
      const wrapper = shallow(<ResultsList {...defaultProps} results={mockResults} />);
      expect(toJson(wrapper)).toMatchSnapshot();
    });
  });

  describe('/LocationMask', () => {
    it('should match the snapshot', () => {
      const wrapper = shallow(<LocationMask onClose={() => {}} />);
      expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('should call a function when the close icon is clicked', () => {
      const onClick = jest.fn();
      const component = shallow(<LocationMask onClose={onClick} />);
      const button = component.find('#t_location_close');
      button.simulate('click');
      expect(onClick).toHaveBeenCalledTimes(1);
    });
  });

  describe('/SearchButton', () => {
    it('should match the snapshot', () => {
      const wrapper = shallow(<SearchButton onClick={() => {}} />);
      expect(toJson(wrapper)).toMatchSnapshot();
    });

    it('should call a function when the button is clicked', () => {
      const onClick = jest.fn();
      const component = shallow(<SearchButton onClick={onClick} />);
      const button = component.find('#t_search_btn');
      button.simulate('click');
      expect(onClick).toHaveBeenCalledTimes(1);
    });
  });
});
