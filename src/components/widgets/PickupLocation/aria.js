export default {
  searchInput: {
    ariaDescribedby: 'fts-pickupLocation-screenReaderInstructions',
    ariaControls: 'fts-pickupLocationResultsContainer',
    ariaAutocomplete: 'list',
    ariaRequired: 'true',
  },
  searchButton: {
    ariaDescribedby: 'geo-tooltip',
  },
};
