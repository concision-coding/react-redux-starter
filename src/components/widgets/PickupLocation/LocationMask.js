import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import CloseIcon from '../../icons/Close';

const Root = styled.div`
  position: absolute;
  bottom: 2px;
  height: 44px;
  overflow: hidden;
  width: 460px;
  background: white;
  left: 2px;
`;

const Title = styled.div`
  font-size: 14px;
`;

const StyledButton = styled.button`
  color: rgb(51, 51, 51);
  width: auto;
  margin: 4px;
  height: 36px;
  border: 1px solid rgb(221, 221, 221);
  border-radius: 4px;
  display: flex;
  font-size: 100%;
  background-color: rgb(238, 238, 238);
  align-items: center;
  outline-style: none;
`;

const IconWrapper = styled.span`
  width: 24px;
  height: 24px;
  cursor: pointer;
`;

const LocationMask = ({ onClose }) => (
  <Root>
    <StyledButton disabled>
      <Title>Your location</Title>
      <IconWrapper
        id="t_location_close"
        onClick={onClose}
        role="presentation"
      >
        <CloseIcon />
      </IconWrapper>
    </StyledButton>
  </Root>
);

LocationMask.propTypes = {
  onClose: PropTypes.func.isRequired,
};


export default LocationMask;
