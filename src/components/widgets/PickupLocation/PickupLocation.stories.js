import React from 'react';
import { storiesOf } from '@storybook/react';
import PickupLocation from './index';
import '../../../App.css';

export const mockResults = [
  {
    country: 'United Kingdom',
    lng: -2.27472,
    city: 'Manchester',
    searchType: 'L',
    alternative: ['GB,UK,England,Manchester Airport'],
    index: 1,
    bookingId: 'airport-38566',
    placeType: 'A',
    placeKey: '1472187',
    iata: 'MAN',
    countryIso: 'gb',
    locationId: '38566',
    name: 'Manchester Airport',
    ufi: 900038550,
    isPopular: true,
    region: 'Greater Manchester',
    lang: 'en',
    lat: 53.3536,
  },
  {
    country: 'United Kingdom',
    lng: -2.23615,
    searchType: 'L',
    alternative: ['GB,UK,England'],
    index: 2,
    bookingId: 'city-2623580',
    placeType: 'C',
    placeKey: '441725',
    countryIso: 'gb',
    locationId: '20951',
    name: 'Manchester',
    ufi: -2602512,
    isPopular: false,
    region: 'Greater Manchester',
    lang: 'en',
    lat: 53.4812,
  },
  {
    country: 'United Kingdom',
    lng: -2.2309,
    city: 'Manchester',
    searchType: 'L',
    alternative: ['GB,UK'],
    index: 3,
    bookingId: 'train-1158',
    placeType: 'T',
    placeKey: '992437021',
    countryIso: 'gb',
    locationId: '129213',
    name: 'Manchester - Piccadilly Train Station',
    ufi: 1158,
    isPopular: false,
    region: 'England',
    lang: 'en',
    lat: 53.4767,
  },
  {
    lng: -4.63451,
    city: 'Derbyhaven',
    searchType: 'L',
    alternative: ['IM'],
    index: 4,
    bookingId: 'airport-19471',
    placeType: 'A',
    placeKey: '1472264',
    iata: 'IOM',
    countryIso: 'im',
    locationId: '19471',
    name: 'Ronaldsway Airport',
    ufi: 900038781,
    isPopular: false,
    region: 'Isle of Man',
    lang: 'en',
    lat: 54.0868,
  },
  {
    country: 'United Kingdom',
    lng: -2.11667,
    searchType: 'L',
    alternative: ['GB,UK,England'],
    index: 5,
    bookingId: 'city-2624987',
    placeType: 'C',
    placeKey: '442845',
    countryIso: 'gb',
    locationId: '21041',
    name: 'Oldham',
    ufi: -2604617,
    isPopular: false,
    region: 'Greater Manchester',
    lang: 'en',
    lat: 53.55,
  },
  {
    country: 'United Kingdom',
    lng: -2.43333,
    searchType: 'L',
    alternative: ['GB,UK,England'],
    index: 6,
    bookingId: 'city-2615373',
    placeType: 'C',
    placeKey: '435024',
    countryIso: 'gb',
    locationId: '20256',
    name: 'Bolton',
    ufi: -2590356,
    isPopular: false,
    region: 'Greater Manchester',
    lang: 'en',
    lat: 53.5833,
  },
];

export const mockNoResults = [
  {
    name: 'No results found',
    index: 1,
  },
];

storiesOf('Components/PickupLocation', module)
  .add('default', () => <PickupLocation />)
  .add('with value', () => <PickupLocation value="manchester" />)
  .add('with error', () => <PickupLocation error="you must allow geolocation" />)
  .add('with user location', () => <PickupLocation hasLocation />)
  .add('with results', () => <PickupLocation results={mockResults} />)
  .add('with results and matching value', () => <PickupLocation value="manc" results={mockResults} />)
  .add('with results and matching value and loader', () => <PickupLocation value="manc" results={mockResults} isLoading />)
  .add('with no results', () => <PickupLocation results={mockNoResults} />);
