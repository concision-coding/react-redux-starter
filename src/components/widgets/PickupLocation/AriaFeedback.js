import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const HiddenText = styled.span`
    position: absolute;
    opacity: 0;
`;

const AriaFeedback = ({ resultsLength }) => (
  <Fragment>
    <HiddenText>
      When autocomplete results are available use up and down arrows to review
      and enter to select. Touch device users, explore by touch or with swipe
      gestures.;
    </HiddenText>
    {resultsLength > 0 ? (
      <HiddenText aria-live="assertive" id="t_aria_text">
        {`${resultsLength} results are available. Keyboard users, use up and down
        arrows to review and enter to select. Touch device users, explore by
        touch or with swipe gestures.`}
      </HiddenText>
    ) : null}
  </Fragment>
);

AriaFeedback.propTypes = {
  resultsLength: PropTypes.number.isRequired,
};

export default AriaFeedback;
