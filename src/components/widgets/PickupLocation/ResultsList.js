import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Result from './Result';

const StyledList = styled.ol`
  box-shadow: rgba(0, 0, 0, 0.2) 0px 1px 2px 0px;
  display: table;
  width: 100%;
  border-radius: 4px;
  overflow: hidden;
`;

const ResultsList = ({ results, onSelect, currentValue, onHover, hoverIndex, showResults }) => (
  <span id="t_aria_wrap" aria-live="polite" aria-expanded={results.length > 0}>
    <div aria-live="assertive">
      <StyledList role="listbox" id="t_list">
        {results.length > 0 && showResults
          ? results.map((result, i) => (
            <Result
              index={i}
              className="t_list_item"
              currentValue={currentValue}
              hoverIndex={hoverIndex}
              clickHandler={onSelect}
              hoverHandler={onHover}
              key={result.index}
              result={result}
            />
          ))
          : null}
      </StyledList>
    </div>
  </span>
);

ResultsList.propTypes = {
  currentValue: PropTypes.string,
  onSelect: PropTypes.func.isRequired,
  results: PropTypes.array.isRequired,
  hoverIndex: PropTypes.number,
  onHover: PropTypes.func.isRequired,
  showResults: PropTypes.bool.isRequired,
};

ResultsList.defaultProps = {
  currentValue: null,
  hoverIndex: 0,
};

export default ResultsList;
