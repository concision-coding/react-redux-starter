import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Spinner from '../../icons/Spinner';
import ToolTip from '../../standard/ToolTip';
import PinIcon from '../../icons/LocationPin';
import aria from './aria';

const StyledButton = styled.button`
  height: 28px;
  width: 28px;
  border: 0 none;
  cursor: pointer;
  outline-style: none;
  background-color: transparent;
  z-index: 1;
  > span {
    position: absolute;
    left: 0;
    top: 0;
    > svg {
      width: 100%;
      height: 100%;
    }
  }

  > span[role='tooltip'] {
    display: none;
    right: -14px;
    left: auto;
    top: -57px;
  }
  &:hover > span[role='tooltip'] {
    display: inline !important;
  }
`;

const StyledLoader = styled.span`
  position: absolute;
  right: 10px;
  top: 10px;
  height: 28px;
  width: 28px;
`;

const SearchButton = ({ onClick, isLoading }) => (isLoading ? (
  <StyledLoader>
    <Spinner />
  </StyledLoader>
) : (
  <StyledButton onClick={onClick} type="button" id="t_search_btn" {...aria.searchButton}>
    <span role="presentation">
      <PinIcon />
    </span>
    <ToolTip role="tooltip">Use your current location</ToolTip>
  </StyledButton>
));

SearchButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
};
SearchButton.defaultProps = {
  isLoading: false,
};

export default SearchButton;
