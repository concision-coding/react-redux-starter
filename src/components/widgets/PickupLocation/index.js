import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import KeyboardEventHandler from 'react-keyboard-event-handler';
import TextInput from '../../standard/TextInput';
import SearchButton from './SearchButton';
import ResultsList from './ResultsList';
import aria from './aria';
import AriaFeedback from './AriaFeedback';
import LocationMask from './LocationMask';

const Root = styled.div`
  max-width: 464px;
`;

const SearchWrapper = styled.div`
  position: relative;
`;

const ErrorMessage = styled.div`
  font-size: 80%;
  padding: 5px 0;
  color: red;
`;

const PickUpLocation = ({
  onChange,
  onLocate,
  onFocus,
  onBlur,
  onSelect,
  onKeyEvent,
  onHover,
  value,
  results,
  hoverIndex,
  error,
  hasLocation,
  onClearLocation,
  showResults,
  isLoading,
}) => (
  <Root>
    <SearchWrapper>
      <KeyboardEventHandler
        handleKeys={['up', 'down', 'enter']}
        onKeyEvent={(key, e) => {
          e.preventDefault();
          return onKeyEvent(key, results, hoverIndex);
        }}
      >
        <TextInput
          id="t_search_input"
          title="Pick-up Location"
          value={value}
          onBlur={onBlur}
          onChange={onChange}
          onFocus={onFocus}
          placeholder="city, airport, station, region, district..."
          autoComplete="off"
          {...aria.searchInput}
          rightButton={<SearchButton onClick={onLocate} isLoading={isLoading} />}
        />
      </KeyboardEventHandler>
      {hasLocation ? <LocationMask id="t_location" onClose={onClearLocation} /> : null}
    </SearchWrapper>
    <ResultsList
      results={results}
      currentValue={value}
      onSelect={onSelect}
      onHover={onHover}
      hoverIndex={hoverIndex}
      showResults={showResults}
    />
    {error !== null ? <ErrorMessage id="t_error">{error}</ErrorMessage> : null}
    <AriaFeedback resultsLength={results.length} />
  </Root>
);

PickUpLocation.propTypes = {
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  onLocate: PropTypes.func.isRequired,
  onHover: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  onKeyEvent: PropTypes.func.isRequired,
  value: PropTypes.string,
  hoverIndex: PropTypes.number.isRequired,
  onClearLocation: PropTypes.func.isRequired,
  results: PropTypes.array,
  error: PropTypes.string,
  hasLocation: PropTypes.bool,
  showResults: PropTypes.bool,
  isLoading: PropTypes.bool,
};

PickUpLocation.defaultProps = {
  results: [],
  value: undefined,
  error: null,
  hasLocation: false,
  showResults: true,
  isLoading: false,
};

export default PickUpLocation;
