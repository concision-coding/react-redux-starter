import React from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';
import Page from './components/layout/Page';
import Header from './components/layout/Header';
import Content from './components/layout/Content';
import './App.css';

const App = ({ route }) => (
  <Page>
    <Header />
    <Content>
      {renderRoutes(route.routes)}
    </Content>
  </Page>
);
App.propTypes = {
  route: PropTypes.shape().isRequired,
};

export default App;
