import React from 'react';
import App from './App';
import Default from './pages/Default';
import NotFound from './pages/NotFound';

export default () => [
  {
    component: App,
    routes: [
      {
        path: '/',
        exact: true,
        render: () => (
          <Default />
        ),
      },
      {
        path: '*',
        exact: true,
        render: () => <NotFound />,
      },
    ],
  },
];
