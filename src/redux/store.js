import { createStore, applyMiddleware } from 'redux';
import { apiMiddleware } from 'redux-api-middleware';
import { composeWithDevTools } from 'redux-devtools-extension';
import validate from './middleware/validate';
import delay from './middleware/delay';
import rootReducer from './root-reducer';
import route from './middleware/route';

export default (state = {}, history = null, mwStart = [], mwEnd = []) => {
  const middlewares = [
    ...mwStart,
    validate,
    delay,
    apiMiddleware,
    route(history),
    ...mwEnd,
  ];

  const store = createStore(
    rootReducer,
    state,
    composeWithDevTools(applyMiddleware(...middlewares)),
  );

  return store;
};
