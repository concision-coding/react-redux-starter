import { combineReducers } from 'redux';
import pickup, * as fromPickup from '../pages/Default/redux/reducer';

export const getPickupDisplayText = state => fromPickup.getDisplayText(state.pickup);
export const getPickupShowResults = state => fromPickup.getShowResults(state.pickup);
export const getPickupLocations = state => fromPickup.getLocations(state.pickup);
export const getPickupLocation = state => fromPickup.getLocation(state.pickup);
export const getPickupHasLocation = state => fromPickup.getHasGeoLocation(state.pickup);
export const getPickupHoverIndex = state => fromPickup.getHoverIndex(state.pickup);
export const getPickupIsLoading = state => fromPickup.getIsLoading(state.pickup);
export const getPickupError = state => fromPickup.getError(state.pickup);

export default combineReducers({
  pickup,
});
