export const VALIDATE = Symbol('VALIDATE');

const dispatchIfExists = (store, action) => {
  if (!action) return null;
  return store.dispatch(action);
};

export default store => next => action => {
  if (action[VALIDATE] === undefined) {
    return next(action);
  }
  const valid = action[VALIDATE].validator(store);
  dispatchIfExists(store, action[VALIDATE].always);
  if (valid) {
    return dispatchIfExists(store, action[VALIDATE].success);
  }
  return dispatchIfExists(store, action[VALIDATE].fail);
};

export const notEmptyString = val => typeof val === 'string' && val.length > 0;
