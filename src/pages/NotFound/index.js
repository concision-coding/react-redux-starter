import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  max-width: 1200px;
  margin: auto;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export default () => (
  <Wrapper>
    <span>Page not found</span>
  </Wrapper>
);
