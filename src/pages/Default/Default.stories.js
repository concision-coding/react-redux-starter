import React from 'react';
import { storiesOf } from '@storybook/react';
import { DefaultPage } from './index';
import '../../App.css';

storiesOf('Pages/Default', module)
  .add('default', () => <DefaultPage />);
