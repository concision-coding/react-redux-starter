import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import PickupLocation from '../../components/widgets/PickupLocation';
import {
  getPickupDisplayText,
  getPickupLocations,
  getPickupHasLocation,
  getPickupError,
  getPickupLocation,
  getPickupHoverIndex,
  getPickupShowResults,
  getPickupIsLoading,
} from '../../redux/root-reducer';
import { setSearch, setLocation, setGeolocation, setFocused, setKeyEvent, setHoverIndex } from './redux/reducer';

const Root = styled.div``;

const Title = styled.h1`
  font-size: 36px;
  width: 464px;
  margin: 0 auto;
  padding: 10px 0 20px 0;
`;

const Subtitle = styled.h2`
  color: rgb(51, 51, 51);
  display: block;
  font-family: 'Ubuntu', 'Open Sans', 'Segoe UI', Tahoma, sans-serif;
  font-size: 24px;
  font-weight: 500;
  height: 32px;
  line-height: 32px;
  margin-bottom: 16px;
  width: 464px;
`;

const StyledForm = styled.form`
  background-color: rgb(243, 206, 86);
  color: rgb(51, 51, 51);
  display: block;
  font-family: 'Open Sans', 'Segoe UI', Tahoma, sans-serif;
  line-height: 20px;
  margin: 0px auto;
  padding: 24px;
  max-width: 512px;
  width: 100%;
`;

export const DefaultPage = ({
  textChangeHandler,
  setFocusHandler,
  setLocationHandler,
  selectPickupHandler,
  clearLocationHandler,
  hoverItemHandler,
  keyEventHandler,
  displayText,
  error,
  searchResults,
  hasGeolocation,
  hoverIndex,
  showResults,
  isLoading,
}) => (
  <Root>
    <Title>BookingGo Technical Test</Title>
    <StyledForm>
      <Subtitle>Let’s find your ideal car</Subtitle>
      <PickupLocation
        onChange={e => textChangeHandler(e.target.value)}
        onBlur={() => setFocusHandler(false, 250)}
        onLocate={setLocationHandler}
        onSelect={selectPickupHandler}
        onFocus={() => setFocusHandler(true)}
        onKeyEvent={keyEventHandler}
        onHover={hoverItemHandler}
        hoverIndex={hoverIndex}
        value={displayText}
        showResults={showResults}
        results={searchResults}
        error={error}
        isLoading={isLoading}
        hasLocation={hasGeolocation}
        onClearLocation={clearLocationHandler}
      />
    </StyledForm>
  </Root>
);

DefaultPage.propTypes = {
  textChangeHandler: PropTypes.func.isRequired,
  setFocusHandler: PropTypes.func.isRequired,
  setLocationHandler: PropTypes.func.isRequired,
  selectPickupHandler: PropTypes.func.isRequired,
  clearLocationHandler: PropTypes.func.isRequired,
  keyEventHandler: PropTypes.func.isRequired,
  hoverItemHandler: PropTypes.func.isRequired,
  hoverIndex: PropTypes.number.isRequired,
  displayText: PropTypes.string,
  error: PropTypes.string,
  searchResults: PropTypes.array,
  hasGeolocation: PropTypes.bool,
  showResults: PropTypes.bool,
  isLoading: PropTypes.bool,
};

DefaultPage.defaultProps = {
  displayText: null,
  error: null,
  searchResults: [],
  hasGeolocation: false,
  showResults: true,
  isLoading: false,
};

const mapStateToProps = state => ({
  selectedLocation: getPickupLocation(state),
  displayText: getPickupDisplayText(state),
  hoverIndex: getPickupHoverIndex(state),
  error: getPickupError(state),
  searchResults: getPickupLocations(state),
  hasGeolocation: getPickupHasLocation(state),
  showResults: getPickupShowResults(state),
  isLoading: getPickupIsLoading(state),
});

const mapDispatchToProps = {
  textChangeHandler: setSearch,
  setFocusHandler: setFocused,
  selectPickupHandler: setLocation,
  hoverItemHandler: setHoverIndex,
  setLocationHandler: () => setGeolocation('a location'),
  clearLocationHandler: () => setGeolocation(''),
  keyEventHandler: setKeyEvent,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DefaultPage);
