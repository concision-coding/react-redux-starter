import { RSAA } from 'redux-api-middleware';
import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';
import { VALIDATE } from '../../../redux/middleware/validate';
import { DELAY } from '../../../redux/middleware/delay';

// TODO -- in a prod build these would be run from an .env
//---
const MAX_RESULTS_NUMBER = 6;
const MIN_VALIDATION_NUMBER = 2;
const BASE_URL = `https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows={${MAX_RESULTS_NUMBER}}}`;
//---

// ACTIONS
export const PICKUP_FETCH_LOCATIONS_START = '@@PICKUP/PICKUP_FETCH_LOCATIONS_START';
export const PICKUP_FETCH_LOCATIONS_SUCCESS = '@@PICKUP/PICKUP_FETCH_LOCATIONS_SUCCESS';
export const PICKUP_FETCH_LOCATIONS_FAILURE = '@@PICKUP/PICKUP_FETCH_LOCATIONS_FAILURE';

export const PICKUP_SET_SEARCH = 'PICKUP_SET_SEARCH';
export const PICKUP_SET_FOCUSED = 'PICKUP_SET_FOCUSED';
export const PICKUP_SET_LOCATION = 'PICKUP_SET_LOCATION';
export const PICKUP_SET_GEOLOCATION = 'PICKUP_SET_GEOLOCATION';
export const PICKUP_KEY_EVENT = 'PICKUP_KEY_EVENT';
export const PICKUP_SET_HOVER_INDEX = 'PICKUP_SET_HOVER_INDEX';

// ACTION CREATORS
export const fetchLocations = (search, apiEndpoint = BASE_URL) => ({
  [RSAA]: {
    endpoint: `${apiEndpoint}&solrTerm={${search}}`,
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
    types: [
      PICKUP_FETCH_LOCATIONS_START,
      PICKUP_FETCH_LOCATIONS_SUCCESS,
      PICKUP_FETCH_LOCATIONS_FAILURE,
    ],
  },
});

export const setSearch = (search, apiEndpoint = BASE_URL) => ({
  [VALIDATE]: {
    validator: () => search && search.length > MIN_VALIDATION_NUMBER,
    success: fetchLocations(search, apiEndpoint),
    always: { type: PICKUP_SET_SEARCH, search },
  },
});

export const setFocused = (isFocused, delay = 0) => ({
  type: PICKUP_SET_FOCUSED,
  isFocused,
  [DELAY]: {
    delay,
  },
});

export const setLocation = location => ({
  type: PICKUP_SET_LOCATION,
  location,
});

export const setGeolocation = geoLocation => ({
  type: PICKUP_SET_GEOLOCATION,
  geoLocation,
});

export const setKeyEvent = (key, results, index) => ({
  type: PICKUP_KEY_EVENT,
  key,
  results,
  index,
});

export const setHoverIndex = index => ({
  type: PICKUP_SET_HOVER_INDEX,
  index,
});

// SELECTORS
export const getDisplayText = state => (
  state.selected && !state.isFocused ? state.selected.name : state.search
);
export const getIsLoading = state => state.isLoading;
export const getShowResults = state => (state.search.length < 3 ? false : state.showResults);
export const getHoverIndex = state => state.hoverIndex;
export const getLocations = state => state.results;
export const getLocation = state => state.selected;
export const getHasGeoLocation = state => state.hasGeolocation;
export const getError = state => state.error;

// REDUCERS
const search = handleActions(
  {
    [PICKUP_SET_SEARCH]: (state, action) => action.search,
    [PICKUP_SET_GEOLOCATION]: () => '',
  },
  '',
);

const showResults = handleActions({
  [PICKUP_SET_FOCUSED]: (state, action) => action.isFocused,
  [PICKUP_SET_LOCATION]: () => false,
  [PICKUP_KEY_EVENT]: (state, action) => {
    if (action.key === 'enter') {
      return false;
    }
    return state;
  },
}, true);

const isLoading = handleActions({
  [PICKUP_FETCH_LOCATIONS_START]: () => true,
  [PICKUP_FETCH_LOCATIONS_SUCCESS]: () => false,
  [PICKUP_FETCH_LOCATIONS_FAILURE]: () => false,
}, false);

const results = handleActions(
  {
    [PICKUP_FETCH_LOCATIONS_SUCCESS]: (state, action) => action.payload.results.docs,
    [PICKUP_FETCH_LOCATIONS_FAILURE]: () => [],
  },
  [],
);

const selected = handleActions(
  {
    [PICKUP_SET_GEOLOCATION]: () => null,
    [PICKUP_SET_LOCATION]: (state, action) => action.location,
    [PICKUP_KEY_EVENT]: (state, action) => {
      if (action.key === 'enter') {
        return action.results[action.index];
      }
      return state;
    },
    [PICKUP_SET_FOCUSED]: (state, action) => (action.isFocused ? null : state),
  },
  null,
);

const error = handleActions(
  {
    [PICKUP_FETCH_LOCATIONS_FAILURE]: () => 'There was an error',
  },
  null,
);

const hasGeolocation = handleActions(
  {
    [PICKUP_SET_GEOLOCATION]: (state, action) => action.geoLocation !== '',
  },
  false,
);

const hoverIndex = handleActions(
  {
    [PICKUP_SET_HOVER_INDEX]: (state, action) => action.index,
    [PICKUP_KEY_EVENT]: (state, action) => {
      if (action.key === 'down') return Math.max(state + 1, 0);
      if (action.key === 'up') return Math.min(state - 1, 5);
      return state;
    },
  },
  0,
);

const isFocused = handleActions(
  {
    [PICKUP_SET_GEOLOCATION]: () => false,
    [PICKUP_SET_FOCUSED]: (state, action) => action.isFocused,
    [PICKUP_KEY_EVENT]: (state, action) => {
      if (action.key === 'enter') {
        return false;
      }
      return state;
    },
  },
  false,
);

export default combineReducers({
  search,
  showResults,
  hoverIndex,
  isLoading,
  isFocused,
  results,
  selected,
  hasGeolocation,
  error,
});
