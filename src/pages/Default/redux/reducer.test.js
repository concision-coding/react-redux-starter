import { RSAA } from 'redux-api-middleware';
import reducer, {
  PICKUP_FETCH_LOCATIONS_START,
  PICKUP_FETCH_LOCATIONS_SUCCESS,
  PICKUP_FETCH_LOCATIONS_FAILURE,
  PICKUP_SET_SEARCH,
  PICKUP_SET_FOCUSED,
  PICKUP_SET_LOCATION,
  PICKUP_SET_GEOLOCATION,
  fetchLocations,
  setSearch,
  setFocused,
  setLocation,
  setGeolocation,
  getDisplayText,
  getLocations,
  getHasGeoLocation,
  getError,
} from './reducer';
import { VALIDATE } from '../../../redux/middleware/validate';
import { DELAY } from '../../../redux/middleware/delay';

describe('Redux/DefaultPage', () => {
  describe('ACTION CREATORS', () => {
    describe('fetchLocations', () => {
      it('should return the expected action', () => {
        const action = fetchLocations('hello world @', 'http://call.me');
        expect(action).toEqual({
          [RSAA]: {
            endpoint: 'http://call.me&solrTerm={hello world @}',
            headers: { 'Content-Type': 'application/json' },
            method: 'GET',
            types: [
              PICKUP_FETCH_LOCATIONS_START,
              PICKUP_FETCH_LOCATIONS_SUCCESS,
              PICKUP_FETCH_LOCATIONS_FAILURE,
            ],
          },
        });
      });
    });

    describe('setSearch', () => {
      it('should return the expected action', () => {
        const action = setSearch('hello world @', 'http://call.me');
        expect(action).toEqual({
          [VALIDATE]: {
            always: { search: 'hello world @', type: PICKUP_SET_SEARCH },
            success: {
              [RSAA]: {
                endpoint: 'http://call.me&solrTerm={hello world @}',
                headers: { 'Content-Type': 'application/json' },
                method: 'GET',
                types: [
                  PICKUP_FETCH_LOCATIONS_START,
                  PICKUP_FETCH_LOCATIONS_SUCCESS,
                  PICKUP_FETCH_LOCATIONS_FAILURE,
                ],
              },
            },
            validator: expect.any(Function),
          },
        });
      });
    });

    describe('setFocused', () => {
      it('should return the expected action', () => {
        const action = setFocused('hello world @', 0);
        expect(action).toEqual({
          isFocused: 'hello world @',
          type: PICKUP_SET_FOCUSED,
          [DELAY]: { delay: 0 },
        });
      });
    });
    describe('setLocation', () => {
      it('should return the expected action', () => {
        const action = setLocation('hello world @');
        expect(action).toEqual({ location: 'hello world @', type: PICKUP_SET_LOCATION });
      });
    });
    describe('setGeolocation', () => {
      it('should return the expected action', () => {
        const action = setGeolocation('hello world @');
        expect(action).toEqual({ geoLocation: 'hello world @', type: PICKUP_SET_GEOLOCATION });
      });
    });
  });

  describe('SELECTORS', () => {
    describe('getDisplayText', () => {
      it('should return search text when focused', () => {
        expect(getDisplayText({ search: 'test', isFocused: true })).toEqual('test');
      });
    });
    describe('getHasGeoLocation', () => {
      it('should return geolocation', () => {
        expect(getHasGeoLocation({ hasGeolocation: true })).toEqual(true);
      });
    });
    describe('getError', () => {
      it('should return an error', () => {
        expect(getError({ error: 'an error' })).toEqual('an error');
      });
    });
  });

  describe('REDUCERS', () => {
    describe('displayText', () => {
      it('should have the correct initial value', () => {
        const state = reducer(undefined, { type: 'no' });
        expect(getDisplayText(state)).toEqual('');
      });
    });
    describe('isFocused', () => {
      it('should have the correct initial value', () => {
        const state = reducer(undefined, { type: 'no' });
        expect((state.isFocused)).toEqual(false);
      });
    });
    describe('locations', () => {
      it('should have the correct initial value', () => {
        const state = reducer(undefined, { type: 'no' });
        expect(getLocations(state)).toEqual([]);
      });
    });
    describe('hasGeolocation', () => {
      it('should have the correct initial value', () => {
        const state = reducer(undefined, { type: 'no' });
        expect(getHasGeoLocation(state)).toEqual(false);
      });
    });
    describe('error', () => {
      it('should have the correct initial value', () => {
        const state = reducer(undefined, { type: 'no' });
        expect(getError(state)).toEqual(null);
      });
    });
  });
});
