import 'react-app-polyfill/ie11';
import 'core-js';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';
import createRoutes from './routes';
import history from './history';
import * as serviceWorker from './serviceWorker';
import createStore from './redux/store';

const store = createStore({}, history);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      {renderRoutes(createRoutes(store))}
    </Router>
  </Provider>,
  document.getElementById('root'),
);

serviceWorker.unregister();
